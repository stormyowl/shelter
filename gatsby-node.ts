import {
  CreateNodeArgs,
  CreatePagesArgs,
  CreateWebpackConfigArgs,
} from "gatsby"
import { createFilePath } from "gatsby-source-filesystem"
import path from "path"

exports.onCreateWebpackConfig = ({
  actions,
  plugins,
  reporter,
}: CreateWebpackConfigArgs) => {
  actions.setWebpackConfig({
    plugins: [
      plugins.provide({
        React: "react",
      }),
    ],
  })

  reporter.info(`Provided React in all files`)
}

exports.onCreateNode = ({
  node,
  getNode,
  actions: { createNodeField },
}: CreateNodeArgs) => {
  if (node.internal.type === "MarkdownRemark") {
    const slug = createFilePath({ node, getNode, basePath: "pages" })
    createNodeField({ node, name: "slug", value: slug })
  }
}

type AllMarkdownRemark = {
  allMarkdownRemark: { edges: { node: { fields: { slug: string } } }[] }
}

exports.createPages = async ({
  graphql,
  actions: { createPage },
}: CreatePagesArgs) => {
  const result = await graphql<AllMarkdownRemark>(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `).then(({ data }) => data)
  result.allMarkdownRemark.edges.forEach(
    ({
      node: {
        fields: { slug },
      },
    }) => {
      createPage({
        path: slug,
        component: path.resolve("./src/templates/page.js"),
        context: { slug }, // Data passed to context is available in page queries as GraphQL variables.
      })
    }
  )
}
