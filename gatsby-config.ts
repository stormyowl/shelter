import dotenv from "dotenv"
import { GatsbyConfig } from "gatsby"
import path from "path"

dotenv.config({
  path: `.env.${process.env.NODE_ENV}`,
})

const config: GatsbyConfig = {
  siteMetadata: {
    title: "Chata na Zagroniu",
    description:
      "Chata na Zagroniu to chatka studencka, która znajduje się w Beskidzie Żywieckim, na grzbiecie poniżej Zapolanki, między Rajczą-Nickuliną, a Złatną, ok. 200 metrów od żółtego szlaku. Chatą opiekuje się Studenckie Koło Przewodników Beskidzkich w Katowicach.",
    author: "Studenckie Koło Przewodników Beskidzkich w Katowicach",
    image: "/og.png",
    siteUrl: "https://chatanazagroniu.pl",
  },
  plugins: [
    {
      resolve: "gatsby-plugin-sass",
      options: {
        sassOptions: {
          includePaths: [path.resolve("src/scss")],
        },
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "src",
        path: path.resolve("src"),
      },
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: path.resolve("src/svg"),
        },
      },
    },
    "gatsby-transformer-json",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "data",
        path: path.resolve("src/data"),
      },
    },
    "gatsby-transformer-sharp",
    "gatsby-plugin-sharp",
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1344,
              linkImagesToOriginal: false,
            },
          },
        ],
      },
    },
    "gatsby-plugin-catch-links",
    {
      resolve: "gatsby-plugin-google-gtag",
      options: {
        trackingIds: ["UA-141891975-1"],
        gtagConfig: {
          anonymize_ip: true,
        },
        pluginConfig: {
          head: true,
          respectDNT: true,
        },
      },
    },
  ],
}

export default config
