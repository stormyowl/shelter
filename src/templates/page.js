import { graphql } from "gatsby"

import { Layout } from "../components/layout/Layout"
import { SEO } from "../components/seo/SEO"

const Page = ({
  data: {
    markdownRemark: {
      frontmatter: { title },
      html,
    },
  },
  location,
}) => (
  <Layout title={title} location={location}>
    <div
      className="content is-medium"
      dangerouslySetInnerHTML={{ __html: html }}
    />
  </Layout>
)

export default Page

export const Head = ({
  data: {
    markdownRemark: {
      frontmatter: { title },
    },
  },
  location,
}) => <SEO title={title} location={location} />

export const query = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
    }
  }
`
