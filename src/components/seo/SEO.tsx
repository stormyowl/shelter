import React, { PropsWithChildren } from "react"

import { useFavicon } from "../../hooks/useFavicon"
import { useHeaderImage } from "../../hooks/useHeaderImage"
import { useSiteMetadata } from "../../hooks/useSiteMetadata"

type SEOProps = PropsWithChildren<{
  title: string
  location: Location
}>

export const SEO = ({ title, location: { pathname }, children }: SEOProps) => {
  const headerImage = useHeaderImage()
  const siteMetadata = useSiteMetadata()
  const { favicon16, favicon32, favicon64 } = useFavicon()

  const { description, author, image, siteUrl } = siteMetadata

  const seo = {
    title: `${title} | ${siteMetadata.title}`,
    description,
    author,
    image: `${siteUrl}${image}`,
    url: `${siteUrl}${pathname}`,
  }

  const cssVars = [
    {
      name: "--fullhd-bg-height",
      value: `${100 / headerImage.fullhd.aspectRatio}%`,
    },
    {
      name: "--desktop-bg-height",
      value: `${100 / headerImage.desktop.aspectRatio}%`,
    },
    {
      name: "--tablet-bg-height",
      value: `${100 / headerImage.tablet.aspectRatio}%`,
    },
    {
      name: "--mobile-bg-height",
      value: `${100 / headerImage.mobile.aspectRatio}%`,
    },
  ]

  return (
    <>
      <title>{seo.title}</title>
      <meta charSet="utf-8" />
      <meta name="description" content={seo.description} />
      <meta property="og:title" content={seo.title} />
      <meta property="og:description" content={seo.description} />
      <meta property="og:type" content="website" />
      <meta property="og:image" content={seo.image} />
      <meta property="og:url" content={seo.url} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="image" content={seo.image} />
      <meta name="twitter:title" content={seo.title} />
      <meta name="twitter:url" content={seo.url} />
      <meta name="twitter:description" content={seo.description} />
      <meta name="twitter:image" content={seo.image} />
      <meta name="twitter:creator" content={seo.author} />
      {[favicon16, favicon32, favicon64].map(({ base64 }) => (
        <link key={base64} rel="shortcut icon" type="image/png" href={base64} />
      ))}
      <style>
        {`
        :root {
          ${cssVars.map(({ name, value }) => `${name}: ${value};`).join("\n")}
        }
      `}
      </style>
      {children}
    </>
  )
}
