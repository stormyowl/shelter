import ImgsViewer from "@deplorable/react-images-viewer"
import ReactPhotoAlbum, { ClickHandler, Photo } from "react-photo-album"
import React, { useState, useCallback } from "react"

import { GalleryProps } from "./Gallery.types"
import { Tile } from "./tile/Tile"

export const Gallery = ({ photos }: GalleryProps) => {
  const [currentImage, setCurrentImage] = useState(0)
  const [viewerIsOpen, setViewerIsOpen] = useState(false)

  const openLightbox: ClickHandler<Photo> = useCallback(({ index }) => {
    setCurrentImage(index)
    setViewerIsOpen(true)
  }, [])

  const closeLightbox = () => {
    setCurrentImage(0)
    setViewerIsOpen(false)
  }

  console.log(photos)

  return (
    <div>
      <ReactPhotoAlbum<Photo>
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        photos={photos.map(({ src, width, height, alt, srcSet }) => ({
          src,
          width,
          height,
          alt,
          srcSet,
        }))}
        layout="rows"
        onClick={openLightbox}
        renderPhoto={Tile}
        spacing={() => 4}
      />
      <ImgsViewer
        imgs={photos.map(({ src, srcSet, alt, thumbnail }) => ({
          src,
          srcSet,
          caption: alt,
          thumbnail,
        }))}
        currImg={currentImage}
        isOpen={viewerIsOpen}
        onClickNext={() => setCurrentImage(currentImage + 1)}
        onNextPrev={() => setCurrentImage(currentImage - 1)}
        onClose={closeLightbox}
        onClickThumbnail={(index: number) => setCurrentImage(index)}
        showThumbnails
        backdropCloseable
      />
    </div>
  )
}
