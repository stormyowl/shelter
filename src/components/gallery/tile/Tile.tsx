import { RenderPhoto } from "react-photo-album"

import { LazyImageContainer } from "../../lazyImage/LazyImageContainer"

export const Tile: RenderPhoto = ({
  imageProps: { src, srcSet, alt },
  layout: { index, width, height },
  layoutOptions: { onClick },
  photo,
}) => (
  <div
    key={index}
    onClick={onClick ? (event) => onClick({ event, photo, index }) : undefined}
    style={{
      width,
      height,
      display: "block",
    }}
    role="presentation"
  >
    <LazyImageContainer
      src={src}
      srcSet={srcSet}
      sizes={`${width}px`}
      alt={alt}
      width={width}
      height={height}
      style={{ ...(onClick ? { cursor: "pointer" } : {}) }}
    />
  </div>
)
