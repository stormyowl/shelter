import { Photo } from "react-photo-album"

type GalleryPhoto = Photo & {
  srcSet: string[]
  thumbnail: string
}

export type GalleryProps<T extends GalleryPhoto = GalleryPhoto> = {
  photos: T[]
}
