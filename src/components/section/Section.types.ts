import { ReactNode } from "react"

export type SectionProps = {
  children?: ReactNode
  className?: string
}
