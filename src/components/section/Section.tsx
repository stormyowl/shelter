import clsx from "clsx"

import { SectionProps } from "./Section.types"

export const Section = ({ children, className }: SectionProps) => (
  <div className={clsx("section", className)}>{children}</div>
)
