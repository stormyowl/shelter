import clsx from "clsx"
import { Formik } from "formik"
import { Form } from "react-bulma-components"

import { ContactFormProps, FormValues } from "./ContactForm.types"
import { options } from "./ContactForm.utils"
import "./ContactForm.scss"

const { Checkbox, Control, Field, Help, Input, Label, Select, Textarea } = Form

export const ContactForm = ({
  initialState,
  onSubmit,
  validationSchema,
}: ContactFormProps) => (
  <Formik<FormValues>
    initialValues={initialState}
    onSubmit={onSubmit}
    validationSchema={validationSchema}
  >
    {(props) => {
      const {
        values,
        touched,
        errors,
        isSubmitting,
        handleChange,
        handleBlur,
        handleSubmit,
      } = props
      return (
        <form onSubmit={handleSubmit}>
          <Field className="is-floating-label">
            <Label htmlFor="name">Imię i nazwisko</Label>
            <Control>
              <Input
                name="name"
                value={values.name}
                color={errors.name && touched.name ? "danger" : undefined}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Control>
            {errors.name && touched.name && (
              <Help color="danger">{errors.name}</Help>
            )}
          </Field>
          <Field className="is-floating-label">
            <Label htmlFor="email">Adres e-mail</Label>
            <Control>
              <Input
                name="email"
                value={values.email}
                type="email"
                color={errors.email && touched.email ? "danger" : undefined}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Control>
            {errors.email && touched.email && (
              <Help color="danger">{errors.email}</Help>
            )}
          </Field>
          <Field className="is-floating-label">
            <Label htmlFor="subject">Temat</Label>
            <Control>
              <Select
                name="subject"
                value={values.subject}
                onChange={handleChange}
                onBlur={handleBlur}
                className="is-fullwidth"
              >
                {options.map(({ value, label }) => (
                  <option value={value} key={value}>
                    {label}
                  </option>
                ))}
              </Select>
            </Control>
            {errors.subject && touched.subject && (
              <Help color="danger">{errors.subject}</Help>
            )}
          </Field>
          <Field className="is-floating-label">
            <Label htmlFor="message">Wiadomość</Label>
            <Control>
              <Textarea
                name="message"
                value={values.message}
                rows={4}
                color={errors.message && touched.message ? "danger" : undefined}
                className="has-fixed-size"
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Control>
            {errors.message && touched.message && (
              <Help color="danger">{errors.message}</Help>
            )}
          </Field>
          <Field>
            <Control>
              <Checkbox
                name="sendCopy"
                checked={values.sendCopy}
                value={values.sendCopy.toString()}
                onChange={handleChange}
                onBlur={handleBlur}
              />{" "}
              Wyślij do mnie kopię wiadomości.
            </Control>
          </Field>
          <button
            type="submit"
            className={clsx("button", "is-link", "is-fullwidth", {
              "is-loading": isSubmitting,
              "is-disabled": isSubmitting || Object.keys(errors).some((x) => x),
            })}
          >
            Wyślij
          </button>
        </form>
      )
    }}
  </Formik>
)
