import axios from "axios"
import { toast } from "react-toastify"
import { object, string } from "yup"

import { FormValues } from "./ContactForm.types"

export const options: { value: number; label: string }[] = [
  { value: 1, label: "Rezerwacja" },
  { value: 2, label: "Chatkowanie" },
  { value: 3, label: "Opinia" },
  { value: 4, label: "Inny" },
]

export const initialState: FormValues = {
  name: "",
  email: "",
  subject: options[0].value,
  message: "",
  sendCopy: true,
}

export function submitWithToken(token: string) {
  return async function ({ subject: subjectId, ...rest }: FormValues) {
    try {
      const subject = options.find(({ value }) => value == subjectId)!.label // eslint-disable-line eqeqeq
      const body = {
        token,
        form: { subject, ...rest },
      }
      const {
        data: { message },
      } = await axios.post("/api/form-submission", body)

      toast.success(message)
    } catch (error) {
      const message =
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        error?.response?.data?.message ||
        "Nie udało się wysłać wiadomości! Może wyślesz wiadomość e-mail?"
      toast.error(message)
    }
  }
}

const validationMessages = {
  required: "To pole jest wymagane.",
  email: "To nie jest poprawny adres e-mail.",
}

export const validationSchema = object().shape({
  name: string().required(validationMessages.required),
  email: string()
    .email(validationMessages.email)
    .required(validationMessages.required),
  subject: string().required(validationMessages.required),
  message: string().required(validationMessages.required),
})
