import { FormikHelpers } from "formik"
import { createRef } from "react"
import ReCAPTCHA from "react-google-recaptcha"
import { ContactForm } from "./ContactForm"
import {
  initialState,
  submitWithToken,
  validationSchema,
} from "./ContactForm.utils"

import { FormValues } from "./ContactForm.types"

export const ContactFormContainer = () => {
  const reCaptcha = createRef<ReCAPTCHA>()

  const onSubmit = async (
    values: FormValues,
    { resetForm }: FormikHelpers<FormValues>
  ) => {
    const token = await reCaptcha.current?.executeAsync()
    await submitWithToken(token!)(values)
    resetForm({})
  }

  return (
    <>
      <ReCAPTCHA
        sitekey={process.env.GATSBY_RECAPTCHA_SITE_KEY_V2 as string}
        size="invisible"
        ref={reCaptcha}
      />
      <ContactForm
        initialState={initialState}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      />
    </>
  )
}
