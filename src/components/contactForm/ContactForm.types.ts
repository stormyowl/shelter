import { FormikConfig } from "formik"

export type FormValues = {
  name: string
  email: string
  subject: number
  message: string
  sendCopy: boolean
}

export type ContactFormProps = Pick<
  FormikConfig<FormValues>,
  "onSubmit" | "validationSchema"
> & {
  initialState: FormValues
}
