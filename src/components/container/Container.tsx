import clsx from "clsx"
import { ContainerProps } from "./Container.types"

export const Container = ({ children, className }: ContainerProps) => (
  <div className={clsx("container", className)}>{children}</div>
)
