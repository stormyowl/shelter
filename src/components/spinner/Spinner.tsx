import clsx from "clsx"

import { SpinnerProps } from "./Spinner.types"
import * as style from "./Spinner.module.scss"

export const Spinner = ({ className }: SpinnerProps) => (
  <div className={clsx(style.spinnerWrapper, className)}>
    <div className={style.spinner}></div>
  </div>
)
