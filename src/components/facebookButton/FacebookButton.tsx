import FacebookIcon from "../../svg/facebook.svg"

import { FacebookButtonProps } from "./FacebookButton.types"
import * as style from "./FacebookButton.module.scss"

export const FacebookButton = ({ href }: FacebookButtonProps) => (
  <a href={href} className="button is-fb is-rounded">
    <span className="icon">
      <FacebookIcon />
    </span>
    <div className={style.btnContent}>Znajdź nas na Facebooku</div>
  </a>
)
