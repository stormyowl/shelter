import { Spinner } from "../spinner/Spinner"

import { LazyImageProps } from "./LazyImage.types"
import * as style from "./LazyImage.module.scss"

export const LazyImage = ({
  width,
  height,
  loading,
  style: customStyle,
  ...props
}: LazyImageProps) => (
  <div>
    {loading && (
      <div className={style.placeholder} style={{ width, height }}>
        <Spinner />
      </div>
    )}
    <img
      style={{ ...customStyle, ...(loading && { visibility: "hidden" }) }}
      {...props}
    />
  </div>
)
