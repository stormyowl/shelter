export type LazyImageContainerProps = {
  src: string
  alt?: string
  srcSet?: string
  sizes?: string
  width: number
  height: number
  style?: Record<string, unknown>
}

export type LazyImageProps = LazyImageContainerProps & {
  loading: boolean
  onLoad: VoidFunction
}
