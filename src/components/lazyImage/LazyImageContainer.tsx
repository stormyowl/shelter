import { useState } from "react"

import { LazyImage } from "./LazyImage"
import { LazyImageContainerProps } from "./LazyImage.types"

export const LazyImageContainer = (props: LazyImageContainerProps) => {
  const [loading, setLoading] = useState(true)

  return (
    <LazyImage {...props} loading={loading} onLoad={() => setLoading(false)} />
  )
}
