import Img, { FluidObject } from "gatsby-image"

import { useHeaderImage } from "../../hooks/useHeaderImage"

import * as style from "./HeaderImage.module.scss"

function srcSetWidthsToPixelDensity({ srcSet, ...rest }: FluidObject) {
  const indexToDensity: Record<number, number> = {
    0: 0.25,
    1: 0.5,
    2: 1,
    3: 1.5,
    4: 2,
    5: 4,
  }
  return {
    srcSet: srcSet
      .split(",")
      .map((x, index) => `${x.trim().split(" ")[0]} ${indexToDensity[index]}x`)
      .join(", "),
    ...rest,
  }
}

function artDirection({
  mobile,
  tablet,
  desktop,
  fullhd,
}: ReturnType<typeof useHeaderImage>) {
  return [
    {
      ...srcSetWidthsToPixelDensity(mobile),
      media: "(max-width: 768px)",
    },
    {
      ...srcSetWidthsToPixelDensity(tablet),
      media: "(min-width: 769px) and (max-width: 1023px)",
    },
    {
      ...srcSetWidthsToPixelDensity(desktop),
      media: "(min-width: 1024px) and (max-width: 1407px)",
    },
    {
      ...srcSetWidthsToPixelDensity(fullhd),
      media: "(min-width: 1408px)",
    },
  ]
}

export const HeaderImage = () => {
  const headerImage = useHeaderImage()

  return (
    <Img
      fluid={artDirection(headerImage)}
      className={style.headerImage}
      critical
      fadeIn={false}
    />
  )
}
