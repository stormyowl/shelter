import { PropsWithChildren } from "react"

export type LayoutProps = PropsWithChildren<{
  title: string
  location: Location
}>

export type QueryReturnValue = {
  allMenuItemsJson: {
    edges: {
      node: { label: string; to: string }
    }[]
  }
}
