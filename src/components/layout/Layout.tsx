import { graphql, useStaticQuery } from "gatsby"

import "../../scss/style.scss"

import { Container } from "../container/Container"
import { Footer } from "../footer/Footer"
import { HeaderImage } from "../headerImage/HeaderImage"
import { NavbarContainer } from "../navbar/NavbarContainer"
import { Section } from "../section/Section"

import { LayoutProps, QueryReturnValue } from "./Layout.types"
import * as style from "./Layout.module.scss"

export const Layout = ({ title, location, children }: LayoutProps) => {
  const {
    allMenuItemsJson: { edges },
  } = useStaticQuery<QueryReturnValue>(query)
  const menuItems = edges.map(({ node: { label, to } }) => ({ label, to }))

  return (
    <div className={style.wrapper}>
      <NavbarContainer entries={menuItems} />
      <HeaderImage />
      <Section className={style.content}>
        <Container>
          {location.pathname !== "/" && <h1 className="title is-1">{title}</h1>}
          {children}
        </Container>
      </Section>
      <Footer className={style.footer}>
        <Container>
          &#169; 2024 Chata na Zagroniu. Wszystkie prawa zastrzeżone.
        </Container>
      </Footer>
    </div>
  )
}

const query = graphql`
  query {
    allMenuItemsJson {
      edges {
        node {
          label
          to
        }
      }
    }
  }
`
