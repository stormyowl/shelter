import { graphql, useStaticQuery } from "gatsby"
import React, { useState } from "react"

import { Navbar } from "./Navbar"
import { NavbarContainerProps, QueryReturnValue } from "./Navbar.types"
import "./Navbar.scss"

export const NavbarContainer = ({ entries }: NavbarContainerProps) => {
  const [isOpened, toggle] = useState(false)
  const {
    brandImage: {
      childImageSharp: { fixed: brandImage },
    },
  } = useStaticQuery<QueryReturnValue>(graphql`
    query {
      brandImage: file(relativePath: { eq: "img/brand.png" }) {
        childImageSharp {
          fixed(height: 36) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return (
    <Navbar
      active={isOpened}
      toggle={() => toggle(!isOpened)}
      brandImage={{ ...brandImage, alt: "Logo chaty na Zagroniu" }}
      entries={entries}
    />
  )
}
