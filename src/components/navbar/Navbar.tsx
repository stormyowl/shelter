import { Link } from "gatsby"
import { Navbar as BulmaNavbar } from "react-bulma-components"

import { Container } from "../container/Container"
import { FacebookButton } from "../facebookButton/FacebookButton"

import { NavbarProps } from "./Navbar.types"

export const Navbar = ({
  active,
  toggle,
  brandImage,
  entries,
}: NavbarProps) => (
  <BulmaNavbar active={active} className="is-sticky">
    <Container>
      <BulmaNavbar.Brand>
        <BulmaNavbar.Item
          renderAs={Link}
          to="/"
          style={{ padding: "0 0.75rem" }}
        >
          <img
            src={brandImage.src}
            srcSet={brandImage.srcSet}
            alt={brandImage.alt}
          />
        </BulmaNavbar.Item>
        <BulmaNavbar.Burger onClick={toggle} />
      </BulmaNavbar.Brand>

      <BulmaNavbar.Menu>
        <BulmaNavbar.Container>
          {entries.map(({ to, label }, key) => (
            <BulmaNavbar.Item renderAs={Link} to={to} key={key}>
              {label}
            </BulmaNavbar.Item>
          ))}
        </BulmaNavbar.Container>
      </BulmaNavbar.Menu>
      {/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
      {/* @ts-ignore */}
      <BulmaNavbar.Container position="end">
        <BulmaNavbar.Item renderAs="div">
          <div className="buttons">
            <FacebookButton href="https://www.facebook.com/chata.na.zagroniu" />
          </div>
        </BulmaNavbar.Item>
      </BulmaNavbar.Container>
    </Container>
  </BulmaNavbar>
)
