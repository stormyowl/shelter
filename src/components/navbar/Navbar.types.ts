import { FixedObject } from "gatsby-image"

export type NavbarContainerProps = {
  entries: { to: string; label: string }[]
}

type GatsbyImageType = {
  childImageSharp: {
    fixed: FixedObject
  }
}

export type QueryReturnValue = {
  brandImage: GatsbyImageType
}

export type NavbarProps = NavbarContainerProps & {
  active: boolean
  toggle: VoidFunction
  brandImage: FixedObject & { alt: string }
}
