import clsx from "clsx"

import { FooterProps } from "./Footer.types"

export const Footer = ({ children, className }: FooterProps) => (
  <div className={clsx("footer", className)}>{children}</div>
)
