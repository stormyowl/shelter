import { graphql, Link } from "gatsby"
import Img from "gatsby-image"
import { ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"

import { ContactFormContainer } from "../components/contactForm/ContactFormContainer"
import { Layout } from "../components/layout/Layout"
import { SEO } from "../components/seo/SEO"

const Kontakt = ({
  location,
  data: {
    logo: {
      childImageSharp: { fluid: logo },
    },
    logoSkpb: {
      childImageSharp: { fluid: logoSkpb },
    },
    logoPttk: {
      childImageSharp: { fluid: logoPttk },
    },
  },
}) => (
  <Layout title="Kontakt" location={location}>
    <ToastContainer
      position="top-center"
      autoClose={3000}
      hideProgressBar
      newestOnTop
      closeOnClick={false}
      rtl={false}
      pauseOnVisibilityChange
      draggable
      pauseOnHover
    />
    <div className="columns">
      <div className="column">
        <ContactFormContainer />
      </div>
      <div className="column">
        <div className="content is-medium">
          <p>
            &ldquo;Chata na Zagroniu&rdquo; jest własnością Oddziału
            Międzyuczelnianego PTTK w Katowicach.
          </p>
          <p>
            Chatą opiekuje się{" "}
            <a href="https://skpb.org">
              Studenckie Koło Przewodników Beskidzkich w Katowicach
            </a>
            . Organem zarządzającym jest Rada Chatki, tel. 538 372 560.
          </p>
        </div>
        <div className="logos">
          <Link to="/" style={wrapperStyle(logo)}>
            <Img
              fluid={logo}
              style={imgStyle(logo)}
              alt="Logo chaty na Zagroniu"
            />
          </Link>
          <a href="https://skpb.org" style={wrapperStyle(logoSkpb)}>
            <Img fluid={logoSkpb} style={imgStyle(logoSkpb)} alt="Logo SKPB" />
          </a>
          <a href="https://pttk.pl" style={wrapperStyle(logoPttk)}>
            <Img fluid={logoPttk} style={imgStyle(logoPttk)} alt="Logo PTTK" />
          </a>
        </div>
      </div>
    </div>
  </Layout>
)

export default Kontakt

export const Head = ({ location }) => (
  <SEO title="Kontakt" location={location} />
)

const wrapperStyle = ({ aspectRatio }) => ({
  flexBasis: aspectRatio * 120,
  flexGrow: 0,
  flexShrink: 1,
})
const imgStyle = ({ aspectRatio }) => ({
  width: "100%",
  height: 0,
  paddingBottom: `${100 / aspectRatio}%`,
})

export const query = graphql`
  query {
    logo: file(relativePath: { eq: "img/logo.png" }) {
      childImageSharp {
        fluid(maxHeight: 120) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    logoSkpb: file(relativePath: { eq: "img/logo-skpb.png" }) {
      childImageSharp {
        fluid(maxHeight: 120) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    logoPttk: file(relativePath: { eq: "img/logo-pttk.png" }) {
      childImageSharp {
        fluid(maxHeight: 120) {
          ...GatsbyImageSharpFluid
        }
      }
    }
  }
`
