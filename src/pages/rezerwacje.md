---
title: Rezerwacje
---

Rezerwacje przyjmuje Jolanta Mazurek pod tel. 538 372 560, pod adresem e-mail [zagron@skpb.org](mailto:zagron@skpb.org) lub przez [formularz kontaktowy](/kontakt/).

<div class="columns is-multiline">
  <div class="column is-full-tablet is-full-desktop is-two-thirds-widescreen is-two-thirds-fullhd is-flex">
    <iframe
      src="https://www.google.com/calendar/embed?showTitle=0&amp;showNav=1&amp;showDate=1&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;hl=pl&amp;bgcolor=%23FFFFFF&amp;src=l5u811nje3ijtl3g4j73l407e8%40group.calendar.google.com&amp;color=%23182C57&amp;ctz=Europe%2FWarsaw"
      title="calendar"
      class="calendar"
    ></iframe>
  </div>
  <div class="column is-full-tablet is-full-desktop is-one-third-widescreen is-one-third-fullhd">

## Legenda

**Chatkowanie** - w chacie znajduje się obsługa, a więc możesz bez wahania zaplanować trasę z noclegiem w naszej chacie. Na pewno zostaniesz przywitany ciepłą herbatką i uśmiechem.

**Rezerwacja** - spodziewamy się osób zapowiedzianych wcześniej, zwykle większej grupy. Jeśli jednak planujesz u nas nocleg z jakąś grupą, weź pod uwagę, że w obecnej sytuacji możemy udzielić noclegu 40 osobom.

  </div>
</div>
