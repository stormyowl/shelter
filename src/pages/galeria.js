import axios from "axios"

import { Gallery } from "../components/gallery/Gallery"
import { Layout } from "../components/layout/Layout"
import { SEO } from "../components/seo/SEO"

const Galeria = ({ location, serverData }) => {
  return (
    <Layout title="Galeria" location={location}>
      <Gallery photos={serverData.images} />
    </Layout>
  )
}

export default Galeria

export const Head = ({ location }) => (
  <SEO title="Galeria" location={location} />
)

export async function getServerData() {
  try {
    const {
      data: {
        result: {
          paging: { total_count, ...paging }, // eslint-disable-line camelcase
          images,
        },
      },
    } = await axios.get(process.env.PIWIGO_API_URL, {
      responseType: "json",
      params: {
        format: "json",
        method: "pwg.categories.getImages",
        cat_id: 1,
      },
    })

    return {
      props: {
        paging: { ...paging, total_count: parseInt(total_count) },
        images: images.map(
          ({
            file: title,
            comment: caption,
            element_url: url,
            width,
            height,
            derivatives: {
              thumb,
              "2small": xxsmall, // eslint-disable-line @typescript-eslint/no-unused-vars
              xsmall,
              small,
              medium,
              large,
              xlarge,
              xxlarge,
            },
          }) => ({
            src: url,
            alt: caption || title,
            srcSet: [xsmall, small, medium, large, xlarge, xxlarge].map(
              ({ url, width, height }) => ({
                src: url,
                width: typeof width === "string" ? parseInt(width) : width,
                height: typeof height === "string" ? parseInt(height) : height,
              })
            ),
            sizes: "(max-width: 1656px) 100vw, 1656px",
            width,
            height,
            aspectRatio: width / height,
            thumbnail: thumb.url,
          })
        ),
      },
    }
  } catch {
    return {
      status: 500,
      headers: {},
      props: {},
    }
  }
}
