---
title: Regulaminy
---

<div class="columns">
  <div class="column">

# Regulamin Chaty na Zagroniu

Chata na Zagroniu jest własnością Oddziału Międzyuczelnianego PTTK w Katowicach.

Chatą opiekuje się Studenckie Koło Przewodników Beskidzkich w Katowicach. Organem zarządzającym jest Rada Chaty.

1. Chata otwarta jest dla każdego Turysty przestrzegającego niniejszego regulaminu.
2. Każdy Gość, pozostający w chatce, zobowiązany jest do zameldowania się u chatkowego i respektowania jego zaleceń. Nocleg podlega opłacie zgodnej z cennikiem.
3. Chata nie prowadzi usług turystycznych ani gastronomicznych. Na terenie posesji i w jej obrębie każdy przebywa dobrowolnie i na własną odpowiedzialność.
4. W chacie należy poruszać się w obuwiu zmiennym. Każdy Gość zobowiązany jest do dbania o czystość i porządek w chacie oraz jej otoczeniu.
5. Posiłki sporządza się wyłącznie w kuchni. Każdy Gość ma prawo do korzystania z infrastruktury kuchennej.
6. Każdy zobowiązany jest do segregowania odpadów.
7. Odpowiedzialność za zniszczenia ponosi sprawca, uiszczając odpowiednio wysoką opłatę, której wysokość odpowiada kosztom naprawy. Wysokość opłaty za usunięcie napisów ze ścian wynosi 250 zł.
8. Turysta zobowiązany jest do pilnowania rzeczy osobistych. Przedmioty pozostawione w chacie są cyklicznie utylizowane.
9. Za osoby nieletnie odpowiadają ich opiekunowie.
10. W chacie obowiązuje całkowity zakaz używania otwartego ognia. Palenie wyrobów tytoniowych i e-papierosów dozwolone jest tylko w wyznaczonym miejscu.
11. Na terenie chatki obowiązuje całkowity zakaz spożywania środków odurzających oraz nadużywania alkoholu.
12. Obecność zwierzęcia na terenie chatki jest dozwolona tylko w obrębie werandy. Za zachowanie zwierzęcia oraz szkody i zanieczyszczenia spowodowane jego obecnością, odpowiada osoba, która zwierzę przyprowadziła.
13. Goście nie przestrzegający regulaminu lub zachowujący się w sposób zakłócający spokój i narażający na niebezpieczeństwo innych będą wydalani z chaty bez prawa do zwrotu uiszczonej opłaty noclegowej.
14. O sprawach nieuregulowanych niniejszymi przepisami decyduje chatkowy. Wszelkie skargi dotyczące obsługi i funkcjonowania chaty należy zgłaszać na bieżąco Radzie Chaty telefonicznie lub w formie pisemnej na adres: Studenckie Koło Przewodników Beskidzkich przy OM PTTK w Katowicach, skr. poczt. 1090, 40-001 Katowice.

  </div>
  <div class="column">

# Regulamin chatkowania

1. Chatkowanie polega na opiece nad obiektem i obsłudze turystów w Chacie na Zagroniu. Do obowiązków chatkowego należą między innymi:

   - obecność w chacie w trakcie chatkowania,
   - dbanie o porządek, ciepło i dobry klimat w chacie,
   - kulturalna i profesjonalna obsługa turystów,
   - dbanie o dobry wizerunek chaty i SKPB Katowice.

2. Chatkować mogą jedna lub dwie osoby. Osoby te, rozumiane jako obsługa (a także ich niepełnoletnie dzieci), nie płacą za noclegi.
3. Chatkować mogą przewodnicy, sympatycy i kursanci SKPB Katowice, oraz osoby związane z chatkami, bazami namiotowymi i kołami przewodnickimi oraz znane w środowisku turystycznym.
4. Aby sprawować opiekę nad obiektem wystarczy jedna osoba z powyższego grona, wówczas to na niej spoczywa odpowiedzialność za funkcjonowanie chaty. Chatkować można np.: z dziewczyną, bratem, ciotką, dziadkiem, kolegą. Oboje traktowani są jako chatkowi.
5. W przypadku, gdy chata nie jest zamknięta pomiędzy dyżurami chatkowi zobowiązani są do porozumienia się ze swoimi poprzednikami i następcami co do planów i pory wymiany obsady w chacie.
6. Chata nie powinna pozostawać pusta i zamknięta w czasie chatkowania, poza przypadkami uzgodnionymi z Radą Chaty.
7. Zapisy na chatkowanie prowadzi wyznaczona osoba z Rady Chaty (informacja na stronie internetowej w zakładce „Rezerwacje”). Termin chatkowania można uznać za zarezerwowany dopiero po potwierdzeniu przez tę osobę.

  </div>
</div>
