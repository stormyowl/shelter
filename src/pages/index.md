---
title: O chacie
---

<div class="columns is-multiline">
  <div class="column is-full-tablet is-one-third-desktop is-one-third-widescreen is-one-third-fullhd">
    <div style="max-width: 350px; margin: 0 auto;">

![Image](../img/logo.png)

</div>

</div>
<div class="column is-full-tablet is-two-thirds-desktop is-two-thirds-widescreen is-two-thirds-fullhd" style="display: flex; flex-direction: column; justify-content: center;">

<h1 class="is-hidden-touch">Chata na Zagroniu</h1>

**Chata na Zagroniu** to chatka studencka, która znajduje się w Beskidzie Żywieckim, na grzbiecie poniżej Zapolanki, między Rajczą-Nickuliną, a Złatną, ok. 200 metrów od żółtego szlaku. W terenie znajdują się stosowne drogowskazy w najważniejszych miejscach.

Chatą opiekuje się [Studenckie Koło Przewodników Beskidzkich w Katowicach](https://skpb.org).

</div>

<div class="column is-full-tablet is-half-desktop is-is-half-widescreen is-is-half-fullhd">

Zapraszamy w:

- **sezonie letnim (lipiec - wrzesień):** zgodnie z [kalendarzem chatkowań](/rezerwacje/),
- **pozostałą część roku:** w weekendy podane w [kalendarzu](/rezerwacje/) **tylko po wcześniejszej [rezerwacji](/kontakt)**.

Oferujemy:

- 40 miejsc noclegowych w warunkach turystycznych,
- miejsca na nocleg w namiotach własnych,
- bezpłatny wrzątek lub herbatę,
- do dyspozycji wyposażoną kuchnię i salon,
- umywalnię w pomieszczeniu gospodarczym,
- miejsce na ognisko przed chatą,
- kominek w salonie,
- w sezonie letnim prysznic na zewnątrz,
- Agricolę, Sabotażystę i inne gry planszowe.

    </div>

    <div class="column is-full-tablet is-half-desktop is-is-half-widescreen is-is-half-fullhd">

  Koszt noclegu w chacie to **20 zł**, w namiocie własnym **7 zł**.

  Respektujemy rabaty PTTK, ponadto oferujemy cenę specjalną **15 zł** dla członków i kursantów Studenckiego Koła Przewodników Beskidzkich w Katowicach oraz „trójkątnych” przewodników, a także dla uczestników przejść przewodnickich innych studenckich kół przewodnickich. Pełny cennik dostępny jest [tutaj](/files/cennik-2022.pdf).

  W chacie obowiązuje [Regulamin Studenckiej Bazy Noclegowej PTTK](/files/sbn.pdf).

  W chacie i jej bliskiej okolicy obowiązuje **całkowity zakaz** nadużywania **alkoholu**.

  Wybierając się do chaty na nocleg, koniecznie pamiętaj o **zamiennym obuwiu**!

    </div>

  </div>
