import { graphql, useStaticQuery } from "gatsby"
import { FixedObject } from "gatsby-image"

type GatsbyImageType = {
  childImageSharp: {
    fixed: FixedObject
  }
}

type ReturnValue = {
  favicon16: GatsbyImageType
  favicon32: GatsbyImageType
  favicon64: GatsbyImageType
}

export const useFavicon = () => {
  const {
    favicon16: {
      childImageSharp: { fixed: favicon16 },
    },
    favicon32: {
      childImageSharp: { fixed: favicon32 },
    },
    favicon64: {
      childImageSharp: { fixed: favicon64 },
    },
  } = useStaticQuery<ReturnValue>(graphql`
    query {
      favicon16: file(relativePath: { eq: "img/favicon.png" }) {
        childImageSharp {
          fixed(width: 16, height: 16) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      favicon32: file(relativePath: { eq: "img/favicon.png" }) {
        childImageSharp {
          fixed(width: 32, height: 32) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      favicon64: file(relativePath: { eq: "img/favicon.png" }) {
        childImageSharp {
          fixed(width: 64, height: 64) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  return { favicon16, favicon32, favicon64 }
}
