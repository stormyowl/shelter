import { graphql, useStaticQuery } from "gatsby"
import { FluidObject } from "gatsby-image"

type GatsbyImageType = {
  childImageSharp: {
    fluid: FluidObject
  }
}

type ReturnValue = {
  mobileImage: GatsbyImageType
  tabletImage: GatsbyImageType
  desktopImage: GatsbyImageType
  fullhdImage: GatsbyImageType
}

export const useHeaderImage = () => {
  const {
    fullhdImage: {
      childImageSharp: { fluid: fullhdImage },
    },
    desktopImage: {
      childImageSharp: { fluid: desktopImage },
    },
    tabletImage: {
      childImageSharp: { fluid: tabletImage },
    },
    mobileImage: {
      childImageSharp: { fluid: mobileImage },
    },
  } = useStaticQuery<ReturnValue>(graphql`
    query {
      fullhdImage: file(relativePath: { eq: "img/header-fullhd.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1920) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      desktopImage: file(relativePath: { eq: "img/header-desktop.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1408) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      tabletImage: file(relativePath: { eq: "img/header-tablet.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1024) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      mobileImage: file(relativePath: { eq: "img/header-mobile.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 768) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  return {
    mobile: mobileImage,
    tablet: tabletImage,
    desktop: desktopImage,
    fullhd: fullhdImage,
  }
}
