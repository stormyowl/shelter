// https://github.com/vercel/vercel/discussions/9637
import axios from "axios/dist/node/axios.cjs"

function formatResponse(response, statusCode, message) {
  console.log(`Returning ${statusCode} status code with "${message}" message.`)
  return response.status(statusCode).json({ message })
}

export default async function ({ method, body }, response) {
  if (method !== "POST") {
    return formatResponse(response, 405, "Method not allowed!")
  }

  console.log("Raw request body:", body)

  const {
    token,
    form: { name, email, subject, message, sendCopy },
  } = body
  console.log("Unpacked token and form's data:", token, {
    name,
    email,
    subject,
    message,
    sendCopy,
  })

  if (!token) {
    console.log("reCAPTCHA token has not been send.")
    return formatResponse(
      response,
      400,
      "Rozwiązanie reCAPTCHY jest obowiązkowym parametrem."
    )
  }

  console.log("Verifying reCAPTCHA token...")

  try {
    const { success } = await axios
      .get("https://www.google.com/recaptcha/api/siteverify", {
        params: {
          secret: process.env.RECAPTCHA_SECRET_KEY_V2,
          response: token,
        },
      })
      .then(({ data }) => data)

    if (!success) {
      return formatResponse(
        response,
        401,
        "Czy na pewno nie jesteś robotem? Jeśli nie to może wyślesz wiadomość e-mail?"
      )
    }
  } catch (error) {
    console.log(error)
    return formatResponse(
      response,
      503,
      "Nie udało się sprawdzić rozwiązania reCAPTCHY."
    )
  }

  const from = JSON.parse(process.env.SENDGRID_FROM)
  const to = JSON.parse(process.env.SENDGRID_TO)
  const bcc = JSON.parse(process.env.SENDGRID_BCC)
  const replyTo = { name, email }

  const payload = {
    personalizations: [
      {
        to: [to],
        ...(sendCopy && { cc: [replyTo] }),
        bcc: [bcc],
        subject,
      },
    ],
    from,
    reply_to: replyTo,
    content: [{ type: "text/plain", value: message }],
  }

  try {
    const headers = { Authorization: `Bearer ${process.env.SENDGRID_API_KEY}` }
    await axios.post("https://api.sendgrid.com/v3/mail/send", payload, {
      headers,
    })

    return formatResponse(response, 200, "Twoja wiadomość została wysłana.")
  } catch (error) {
    console.log("SendGrid error:", {
      status: error.response.status,
      data: error.response.data,
    })
    return formatResponse(response, 503, error.response)
  }
}
